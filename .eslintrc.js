module.exports = {
  'env': {
    'es6': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'plugins': [
    'import',
    '@typescript-eslint'
  ],
  'rules': {
    //general rules
    'indent': ['error', 2, { 'VariableDeclarator': { 'var': 2, 'let': 2, 'const': 3 }, 'SwitchCase': 1 } ],
    'semi': [2, 'never'],
    'one-var': 0,
    'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
    'no-underscore-dangle': 0,
    'prefer-const': 2,
    'no-undef': 0,
    'max-len': 0,
    'func-names': ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    'prefer-arrow-callback': 2,
    'require-atomic-updates': 0,
    'quotes': [2, 'single', 'avoid-escape'],

    //import rules
    'import/prefer-default-export': 0,
    'import/imports-first': 2,
    'import/no-extraneous-dependencies': 0,
    'import/no-unresolved': 0,
    'import/no-commonjs': [2, 'allow-primitive-modules'],

    //typescript rules
    '@typescript-eslint/array-type': 2,
    '@typescript-eslint/no-empty-interface': 2,
    '@typescript-eslint/no-empty-function': 2,
    '@typescript-eslint/no-for-in-array': 2,
    '@typescript-eslint/no-require-imports': 2,
    // "@typescript-eslint/ban-ts-ignore": 2,
    '@typescript-eslint/no-unused-vars': 2
  }
}
