import 'module-alias/register'
import http from 'http'
import express from 'express'
import KafkaHelper from '@shared/kafka'
import * as User from '@handlers/user'
import { Database } from '@db'

import debugjs from 'debugjs-wrapper'
const { debug, error } = debugjs.all('core:worker')
const PORT = process.env.PORT ? process.env.PORT : 5001


const startServer = async () => {

  // await Database.connect()

  global.kafka = new KafkaHelper()

  global.kafka.init({
    clientId: 'kafka-admin',
    brokerList: 'localhost:9092',
    groupId: 'kafka'
  })

  global.kafka.subscribe('topic1', User.save)

  const app = express()

  app.get('/healthz', (req, res) => res.status(200).send('Live!'))
  app.get('/1', (req, res) => {
    global.kafka.produce('topic1', 'Hello topic111', null)
  })


  const server: http.Server = http.createServer(app)
  server.listen(PORT, () => {
    debug('Listening at port:', PORT)
  })

}

startServer()