import Kafka from 'node-rdkafka'

import debugjs from 'debugjs-wrapper'
const { debug, error } = debugjs.all('core:shared:kafka')

class KafkaHelper {
  private _consumer
  private _producer
  private _client
  private _subscribers = new Map()

  public init(conf){

    this._client = Kafka.AdminClient.create({
      'client.id': conf.clientId,
      'metadata.broker.list': conf.brokerList
    })

    this._consumer = new Kafka.KafkaConsumer({
      'group.id': conf.groupId,
      'metadata.broker.list': conf.brokerList
    }, {})

    this._producer = new Kafka.Producer({
      'metadata.broker.list': conf.brokerList,
      'dr_cb': true
    })

    this._consumer
      .on('ready', () => {
        this._consumer.subscribe(Array.from(this._subscribers.keys()))
        this._consumer.consume()
      })
      .on('data', msg => {
        const handler = this._subscribers.get(msg.topic)
        handler(msg.value.toString())
      })

    this._producer.on('event.error', (err) => {
      error('Error from producer')
      error(err)
    })


    this._producer.setPollInterval(100)
    this._producer.connect()
    this._consumer.connect()

  }

  public subscribe(topic, handler){
    this._subscribers.set(topic, handler)
    this._client.createTopic({
      topic: topic,
      num_partitions: 1,
      replication_factor: 1
    }, (err) => {
      if(err) error(err.message)
      else debug('Created new topic', topic)
    })
  }

  public produce(topic, params, key){
    try {
      this._producer.produce(
        topic,
        null,
        Buffer.from(JSON.stringify(params)),
        key,
        Date.now(),
        'someToken'
      )
    } catch (err) {
      error('A problem occurred when sending our message')
      error(err)
    }
  }
}
export default KafkaHelper