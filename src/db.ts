import mongoose from 'mongoose'

export class Database {
  static async connect() {
    
    try {
      await mongoose.connect(
        process.env.MONGODB_URI,
        {
          reconnectTries: Number.MAX_VALUE,
          useNewUrlParser: true
        }
      )
      
      console.log('Connected to mongo DB')
    } catch (err) {
      console.error('Failed to connect to mongo DB', err)
    }
    
  }
}
